# fin-will-be-leg

A script to find out how this meme can be applied to english words.

```
grep -i fin /usr/share/dict/words | sed 's/[fF]in/leg/' | sort | comm -12 <(sort < /usr/share/dict/words) -
```

## Output

```
leg
leg's
legal
legal's
legality
legality's
legalize
legalized
legalizes
legalizing
legally
legals
legs
```
